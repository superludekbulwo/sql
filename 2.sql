SELECT co.name
FROM country co
WHERE (SELECT count(1) FROM city ci JOIN building b ON b.cityID = ci.cityID WHERE ci.countryID = co.countryID) = 0;
